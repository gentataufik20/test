var app = angular.module('App',[]);

app.controller('homeController', function ($scope,$http) {

	$scope.displayMode = 'list';
	$scope.singelData = null;

	$scope.listDriver = function () {
		$http.get('data/drivers.json')
			.success(function (data) {
				$scope.drivers = data;
			});
	};

	$scope.listDriver();
})